<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Flight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flight', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_id');
            $table->foreign('item_id')->references('id')->on('items');
            $table->unsignedInteger('from_province_id');
            $table->foreign('from_province_id')->references('id')->on('province');
            $table->unsignedInteger('to_province_id');
            $table->foreign('to_province_id')->references('id')->on('province');
            $table->boolean('one_way')->default(false);
            $table->boolean('two_way')->default(false);
            $table->integer('adults');
            $table->integer('childrenn');
            $table->integer('Youths');
            $table->integer('seat_infants');
            $table->integer('lap_infants');
            $table->integer('class')->default('1');
            $table->string('airport')->default('null');
            $table->unsignedInteger('company_flight_id');
            $table->foreign('company_flight_id')->references('id')->on('company_flight');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flight');
    }
}
