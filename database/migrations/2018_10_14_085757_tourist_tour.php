<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TouristTour extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tourist_tour', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_id');
            $table->foreign('item_id')->references('id')->on('items');
            $table->integer('hotel_rate')->unsigned()->default(0);
            $table->integer('duration')->unsigned()->default(0);
            $table->string('date_of_tour')->default('day');
            $table->boolean('free_children')->default(false);
            $table->boolean('breakfast_included')->default(false);
            $table->boolean('dinner_included')->default(false);
            $table->boolean('launch_included')->default(false);
            $table->boolean('open_food')->default(false);
            $table->unsignedInteger('time_to_open_id');
            $table->foreign('time_to_open_id')->references('id')->on('time_to_open');
            $table->unsignedInteger('to_country_id');
            $table->foreign('to_country_id')->references('id')->on('country');
            $table->unsignedInteger('to_province_id');
            $table->foreign('to_province_id')->references('id')->on('province');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tourist_tour');
    }
}
