<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtraDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('extra_details', function (Blueprint $table) {
           $table->increments('id');
           $table->string('key');
           $table->string('value');
           $table->unsignedInteger('item_id');
           $table->foreign('item_id')->references('id')->on('items');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_details');
    }
}
