<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cinema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('movie', function (Blueprint $table) {
           $table->increments('id');
           $table->unsignedInteger('item_id');
           $table->foreign('item_id')->references('id')->on('items');
           $table->string('main_actor')->default('nop');
           $table->date('movie_date');
           $table->boolean('food_included')->default(false);
           $table->boolean('vip')->default(false);
           $table->boolean('3d')->default(false);
           $table->boolean('normal')->default(false);
           $table->unsignedInteger('time_to_open_id');
           $table->foreign('time_to_open_id')->references('id')->on('time_to_open');
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('movie');
    }
}
