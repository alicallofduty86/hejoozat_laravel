<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HotelPark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel_park', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_id');
            $table->foreign('item_id')->references('id')->on('items');
            $table->integer('adults');
            $table->integer('childrenn');
            $table->integer('number_of_room');
            $table->boolean('family_room')->default(false);
            $table->boolean('double_bed')->default(false);
            $table->boolean('twin_beds')->nullable()->default(false);
            $table->boolean('swimming_pool')->default(false);
            $table->boolean('sauna')->default(false);
            $table->boolean('breakfast_included')->default(false);
            $table->boolean('fitness_center')->default(false);
            $table->boolean('dinner_included')->default(false);
            $table->boolean('self_catering')->default(false);
            $table->boolean('free_wifi')->default(false);
            $table->boolean('parking')->default(false);
            $table->boolean('shuttle')->default(false);
            $table->boolean('private_bathroom')->default(false);
            $table->boolean('balcony')->default(false);
            $table->boolean('flat_screen_tv')->default(false);
            $table->boolean('air_conditioning')->default(false);
            $table->boolean('hairdryer')->default(false);
            $table->boolean('wellness_centre')->default(false);
            $table->boolean('upper_floors_accessible_by_lift')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
