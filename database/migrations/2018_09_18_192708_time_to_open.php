<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TimeToOpen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_to_open', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('day_to_open_id');
            $table->foreign('day_to_open_id')->references('id')->on('day_to_open');
            $table->float('from_time')->default(1);
            $table->float('to_time')->default(24);
            $table->boolean('every_time')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_to_open');
    }
}
