<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hejoozat</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Cairo:600');
            .content {
                text-align: center;
            }
            .title {
                font-size: 84px;
            }
            }
            .navbar-collapse{
                color: #ffe01a !important;
            }

                .btn-primary {
                background: #FFDD00;
                color: #fff;
                border: 1px solid #FFDD00; }
                .btn-primary:hover, .btn-primary:focus, .btn-primary:active {
                background: #ffe01a !important;
                border-color: #ffe01a !important;
                color: #2C2E3E !important; }
                .btn-primary.btn-outline {
                background: transparent;
                color: #FFDD00;
                border: 1px solid #FFDD00; }
                .btn-primary.btn-outline:hover, .btn-primary.btn-outline:focus, .btn-primary.btn-outline:active {
                    background: #FFDD00;
                    color: #fff; }
            * {

                font-family: 'Cairo', sans-serif;
            }
            body { background-color: #f9f4f4;;
                   color: #636b6f;
                   margin: 0;
                   padding-bottom: 100px;
                   min-height:967px;
                   position:relative;
                 }
                 .text-muted{
                    color: #28a745 !important;
                    font-family: arabic1;

                }

                .rate{
                    color: #f1c40f;
                }
                .full {
    width: 100%;	
}

.footer {
	height: auto;
	
    position: absolute;
    left: 0;
    bottom: 0;
    color:#fff;
	border-bottom: 1px solid #CCCCCC;
	border-top: 1px solid #DDDDDD;
    background: #343a40!important; 
}

.footer-bottom {
    background: #343a40!important; 
	border-top: 1px solid #DDDDDD;
	padding-top: 20px;
	padding-bottom: 20px;
    color:#fff;
}
</style>
    </head>
<body>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">


            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                @auth('client')
                  
                   
                   <div class="dropdown">
                        <button class="btn btn-primary btn-outline my-2 my-sm-0 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{ Auth::guard('client')->user()->name }} <span class="caret"></span>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ route('clientLogout') }}"
                                onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                 تسجيل خروج
                             </a>
                          <form id="logout-form" action="{{ route('clientLogout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                      </div>
                  @else
                  <form class="form-inline my-2 my-lg-0 mr-auto" method="POST" action="{{ route('clientLogin') }}">
                        @csrf
                        <input class="form-control mr-sm-2" name="password"  type="password" placeholder="كلمة المرور" dir="rtl" style="border: solid 1px #666;height: 43px;">
                        <input class="form-control mr-sm-2 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus  type="email" placeholder="ألأيميل" dir="rtl" style="border: solid 1px #666;height: 43px;">
                        <button class="btn btn-primary btn-outline my-2 my-sm-0" type="submit"><i class="fas fa-sign-in-alt"></i>&nbsp;&nbsp;&nbsp;تسجيل دخول</button>
                      </form>
                  @endauth
              <ul class="navbar-nav ml-auto" style="color: #ffe01a !important;">
                    @auth('client')
                    <li class="nav-item "  >
                        <form action="{{route('bookedList')}}" method="get">
                                <button type="submit" class="btn btn-outline-danger btn-outline">
                                <span class="badge badge-light" style="font-size: 93%;">{{Session::get('count')}}</span>
                                    &nbsp;
                                    <i class="fas fa-cart-arrow-down fa-x fa-flip-horizontal"></i>
                                </button>
                        </form>

                        </li>
                        &nbsp;
                        &nbsp;
                          <li class="nav-item" >
                            <a class="btn btn-primary btn-outline "  ><i class="fas fa-user-circle"></i>&nbsp;&nbsp;&nbsp;حسابي</a>
                        </li>
                        &nbsp;
                        &nbsp;
                        <li class="nav-item" >
                          <a class="btn btn-primary btn-outline "  ><i class="fas fa-phone-square"></i>&nbsp;&nbsp;&nbsp;أتصل بنا</a>
                      </li>
                      &nbsp;
                      &nbsp;
                      <li class="nav-item" >
                        <a class="btn btn-primary btn-outline "  ><i class="fas fa-ban"></i>&nbsp;&nbsp;&nbsp;من نحن</a>
                    </li>
                    &nbsp;
                    &nbsp;
                    @else 
                    <li class="nav-item" >
                            <a class="btn btn-primary btn-outline "  ><i class="fas fa-phone-square"></i>&nbsp;&nbsp;&nbsp;أتصل بنا</a>
                        </li>
                        &nbsp;
                        &nbsp;
                        <li class="nav-item" >
                          <a class="btn btn-primary btn-outline "  ><i class="fas fa-ban"></i>&nbsp;&nbsp;&nbsp;من نحن</a>
                      </li>
                      &nbsp;
                      &nbsp;
                     <li class="nav-item " >
                            <a class="btn btn-primary btn-outline" hidden ><i class="fas fa-user-plus"></i>&nbsp;&nbsp;&nbsp;أنشاء حساب</a>
                        </li>
                    @endauth
              </ul>
          
            </div>
            &nbsp;
            &nbsp;
            &nbsp;
        <a class="navbar-brand" href="{{route('home')}}">حجوزات</a>
          
          </nav>
          
          <!-- Button trigger modal -->
          <button type="button" id="noty" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" hidden>
              Launch demo modal
            </button>
            
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header text-center" style="background: #bbb;">
                    <h5 class="modal-title" id="exampleModalLabel" style="margin-left: auto;"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body" style="text-align: center;color: red;">
                    <h3>
                      <i class="fas fa-exclamation"></i>
                      &nbsp;&nbsp;
                      
                    </h3>
          
                  </div>
                </div>
              </div>
            </div>
        
         <div class="content">
          @yield('content')
        </div>
        <br>
        <br>
        <footer style="position: absolute;width: 100%;bottom: 0;">
                
                <div class="footer-bottom">
                    <div class="container text-center">
                        <p class=""> Copyright <i class="far fa-copyright"></i> 2018-2019. All rights reserved for enjaz company .  </p>
                    </div>
                </div>
               
            </footer> 
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
       
<script>
        @if(Session::has('Title'))
        toastr["error"]("تأكد من الايميل و كلمة المرور", "مشكلة في تسجيل الدخول")

        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        }
        console.log('dfgggggggdfmkngdfnglkdfngdflkglkgkldfjgklfdjgkfdl0000000000000000')
        @endif

        @if(Session::has('success'))
        toastr["success"]("{{Session::get('success')}}", "تمت العملية بنجاح")

        toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
        }
        @endif

      </script>
    </body>
</html>
