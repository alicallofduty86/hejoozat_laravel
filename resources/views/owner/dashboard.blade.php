@extends('layouts\layout1')

@section('content')
<link rel="stylesheet" href="vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="vendors/css/vendor.bundle.addons.css">
<br>
<style>
@import url('https://fonts.googleapis.com/css?family=Cairo:600');
* {

font-family: 'Cairo', sans-serif;
}
.clearfix > .float-left{
  font-size: 235%;
}
</style>
<div class="container">
        <div class="alert alert-success" role="alert">
                <h4 class="alert-heading"> التقارير و الأحصائيات الأسبوعية</h4>
              </div>
              <br>
              <br>
              <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                      <div class="card card-statistics">
                        <div class="card-body">
                          <div class="clearfix">
                            <div class="float-left">
                              <i class="mdi mdi-cube text-danger icon-lg"></i>
                            </div>
                            <div class="float-right">
                              <p class="mb-0 text-right">الاموال المستحصلة</p>
                              <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">$65,650</h3>
                              </div>
                            </div>
                          </div>
                          <p class="text-muted mt-3 mb-0 float-right" dir="rtl">
                            <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> 65% أنخفاض في المبيعات
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                      <div class="card card-statistics">
                        <div class="card-body">
                          <div class="clearfix">
                            <div class="float-left">
                              <i class="mdi mdi-receipt text-warning icon-lg"></i>
                            </div>
                            <div class="float-right">
                              <p class="mb-0 text-right">الطلبات</p>
                              <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">3455</h3>
                              </div>
                            </div>
                          </div>
                          <p class="text-muted mt-3 mb-0 float-right" dir="rtl">
                            <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> أحصائية اخر اسبوع
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                      <div class="card card-statistics">
                        <div class="card-body">
                          <div class="clearfix">
                            <div class="float-left">
                              <i class="mdi mdi-poll-box text-success icon-lg"></i>
                            </div>
                            <div class="float-right">
                              <p class="mb-0 text-right">المبيعات</p>
                              <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">5693</h3>
                              </div>
                            </div>
                          </div>
                          <p class="text-muted mt-3 mb-0 float-right" dir="rtl">
                            <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> التحصيل الاسبوعي
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                      <div class="card card-statistics">
                        <div class="card-body">
                          <div class="clearfix">
                            <div class="float-left">
                              <i class="mdi mdi-close popup-dismiss d-none d-md-block text-danger icon-lg"></i>
                            </div>
                            <div class="float-right">
                              <p class="mb-0 text-right">الطلبات الملغاة</p>
                              <div class="fluid-container">
                                <h3 class="font-weight-medium text-right mb-0">246</h3>
                              </div>
                            </div>
                          </div>
                          <p class="text-muted mt-3 mb-0 float-right" dir="rtl">
                            <i class="mdi mdi-reload mr-1" aria-hidden="true"></i> التحصيل الاسبوعي
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <br>
                  <br>
        <div class="row">
                <div class="col-sm-6">
                      <div class="card">
                          <div class="card-body"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                              <h4 class="card-title">الاحصائيات الشهرية للمبيعات</h4>
                              <canvas id="lineChart" style="height: 261px; display: block; width: 523px;" width="523" height="261" class="chartjs-render-monitor"></canvas>
                          </div>
                      </div>
                </div>
              
                <div class="col-sm-6">
                      <div class="card">
                              <div class="card-body"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                                <h4 class="card-title">الاحصائيات السنوية للمبيعات</h4>
                                <canvas id="barChart" style="height: 261px; display: block; width: 523px;" width="523" height="261" class="chartjs-render-monitor"></canvas>
                              </div>
                            </div>
                  </div>
              </div>
</div>




      <script src="vendors/js/vendor.bundle.base.js"></script>
      <script src="vendors/js/vendor.bundle.addons.js"></script>
      <!-- endinject -->
      <!-- Plugin js for this page-->
      <!-- End plugin js for this page-->
      <!-- inject:js -->
      <script src="js/off-canvas.js"></script>
      <script src="js/misc.js"></script>
      <!-- endinject -->
      <!-- Custom js for this page-->
      <script src="js/chart.js"></script>

@stop