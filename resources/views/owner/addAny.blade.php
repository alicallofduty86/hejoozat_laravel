@extends('layouts\layout1')

@section('content')

<style>
.form-check > .row{
    background:rgb(219, 253, 151);
    padding: 4px;
    text-align: right;
    border-radius: 5px;
    border: 1px solid #c1c2c2;
    color:#000;
    word-wrap: normal;
    font-size: 90%;
}
</style>
<br>
<div class="container img-thumbnail" style="background: #ecf0f1;padding: 20px;margin-top: 20px;padding-right: 40px" >
    <br>
    <div class="alert alert-danger text-right" role="alert" dir="rtl">
            <h4>أملا الحقول التالية .........</h4>
    </div>
@if (session('success'))
<h4>{{session('success')}}</h4>
@endif
@if (session('error'))
<h4>{{session('error')}}</h4>
@endif
<hr>
<form method="POST" action="{{ route('addAny') }}" enctype="multipart/form-data">
@csrf
<div class="row">
  
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
        <div class="input-group mb-3">
            <input type="number" name="maxRequest" class="form-control text-right" placeholder="" aria-label="" aria-describedby="basic-addon2" >
            <div class="input-group-append">
              <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">اقصى عدد طلبات</span>
            </div>
          </div>
      </div>
  <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
      <div class="input-group mb-3">
          <input type="number" name="number" class="form-control text-right" placeholder="" aria-label="" aria-describedby="basic-addon2" >
          <div class="input-group-append">
            <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">العدد</span>
          </div>
        </div>
    </div>

      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
        <div class="input-group mb-3">
            <input type="text" name="name" class="form-control text-right" placeholder="" aria-label="" aria-describedby="basic-addon2" >
            <div class="input-group-append">
              <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">أسم الماد</span>
            </div>
          </div>
    </div>
</div>

<hr>
<div class="row">
  
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="input-group mb-3">
                    <input type="number" name="deduction"  class="form-control text-right" placeholder="" aria-label="" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">المبلغ مقدما</span>
                    </div>
                  </div>
    </div>
    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="input-group mb-3">
                    <input type="number" name="price"  class="form-control text-right" placeholder="" aria-label="" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">السعر</span>
                    </div>
                  </div>
      </div>
      <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="form-group">
                    <select class="form-control " name="category"  id="exampleFormControlSelect1" dir="rtl" style="background:rgb(219, 253, 151);font-size: 90%">
                      <option selected>حدد التصنيف</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </select>
                  </div>
        </div>
  </div>


    <hr>
    <div class="row">
      
        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <div class="form-check">
              
                <div class="row" >
                    <div class=" col-lg-9 col-xs-9 col-sm-9 col-md-9 text-right">
                        <label class="form-check-label" for="defaultCheck1">
                            حجز بدون كارد
                          </label>
                      </div>
                  <div class=" col-lg-3 col-xs-3 col-sm-3 col-md-3">
                      <input class="form-check-input" name="withoutCredit"  type="checkbox" value="" id="defaultCheck111">
                  </div>
                </div>
                </div>
        </div>


        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <div class="form-check">
              
                <div class="row" >
                    <div class=" col-lg-9 col-xs-9 col-sm-9 col-md-9 text-right">
                        <label class="form-check-label" for="defaultCheck1">
                           أمكانية الالغاء 
                          </label>
                      </div>
                  <div class=" col-lg-3 col-xs-3 col-sm-3 col-md-3">
                      <input class="form-check-input" name="freeCancellation"  type="checkbox" value="" id="defaultCheck222">
                  </div>
                </div>
                </div>
        </div>


        <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2">
            <div class="form-check">
              
                <div class="row">
                    <div class=" col-lg-9 col-xs-9 col-sm-9 col-md-9 text-right">
                        <label class="form-check-label" for="defaultCheck1">
                           دفع مسبق
                          </label>
                      </div>
                  <div class=" col-lg-3 col-xs-3 col-sm-3 col-md-3">
                      <input class="form-check-input" name="noPrepayment"  type="checkbox" value="" id="defaultCheck333">
                  </div>
                </div>
                </div>
          </div>


        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="input-group mb-3">
                <input type="number" name="cancellationDeduction"  class="form-control text-right" placeholder="" aria-label="" aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">نسبة الاستقطاع في حالة الالغاء</span>
                </div>
              </div>
          </div>






          
        
      </div>
            <hr>
            
            <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                            <div class="form-group" dir="rtl">
                                    <label class="btn btn-outline-secondary btn-block btn-file">
                                        حمل الصورة 4 <input name="image4" type="file" style="display: none;">
                                    </label>
                                    
                                  </div>
                                  <img   id="image4" src="../assets/img/3.png" alt="" style="width: 100%;height: 100%;display: none" >
                        </div>
                        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group" dir="rtl">
                                        <label class="btn btn-outline-secondary btn-block btn-file">
                                            حمل الصورة 3 <input name="image3" type="file" style="display: none;">
                                        </label>
                                        
                                      </div>
                                      <img   id="image3" src="../assets/img/3.png" alt="" style="width: 100%;height: 100%;display: none" >
                          </div>
                          <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                <div class="form-group" dir="rtl">
                                        <label class="btn btn-outline-secondary btn-block btn-file">
                                            حمل الصورة 2 <input name="image2" type="file" style="display: none;">
                                        </label>
                                        
                                      </div>
                                      <img   id="image2" src="../assets/img/3.png" alt="" style="width: 100%;height: 100%;display: none" >
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                    <div class="form-group" dir="rtl">
                                            <label class="btn btn-outline-secondary btn-block btn-file">
                                                حمل الصورة 1 <input name="image1" type="file" style="display: none;">
                                            </label>
                                            
                                          </div>
                                          <img   id="image1" src="../assets/img/3.png" alt="" style="width: 100%;height: 100%;display: none" >
                              </div>
            </div>
            <hr>
            
            
            <div class="row">
              
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                  <div class="form-group">
                      <label for="exampleFormControlTextarea1">ملاحظات</label>
                      <textarea class="form-control" name="disc"  id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
              </div>
              
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                              <button type="submit" class="btn  btn-success" ><i class="fas fa-user-plus"></i>&nbsp;&nbsp;حفظ 
                                </button>
                </div>
              </div>
            
            

</form>
</div>

@stop