@extends('layouts\layout1')

@section('content')

<br>
<div class="container" >
        
   <div class="alert alert-success align-right" role="alert">
       <div class="row">
            <div class="col-md-10">
                    <h5 class="alert-heading" style="float: right;"> فلتر /الكل /الموافق عليها /قيد التسليم / الملغاة </h5>
            </div>
           <div class="col-md-2">
                <div class="dropdown open" >
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="triggerId" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="fas fa-filter"></i>
                                &nbsp;
                                &nbsp;
                                    فلتر
                                </button>
                        <div class="dropdown-menu" aria-labelledby="triggerId">
                            <button class="dropdown-item" href="#">الكل</button>
                            <button class="dropdown-item " href="#">الموافق عليها</button>
                            <button class="dropdown-item " href="#">قيد التسليم</button>
                            <button class="dropdown-item " href="#">الملغاة</button>
                        </div>
                    </div>
           </div>

       </div>

   </div>

        <div class="table-responsive" style="background: #ecf0f1;">
            <table class="table table-bordered">
                <thead class="" style="background-color: #343a40!important;color: aliceblue">
                    <tr>
                        <th class="text-center" >حالة الحجز</th>
                        <th class="text-center" >-----</th>
                        <th class="text-center">وقت الحجز</th>
                        <th class="text-center">تاريخ الحجز</th>
    
                       <!-- <th class="text-center">المبلغ الدفوع</th> -->
    
                        <th class="text-center">السعر</th>
        
                        <th class="text-center">العدد</th>
                        <th class="text-center">المادة</th>
                        <th class="text-center">Order_QR</th>
       
                        
                    </tr>
                </thead>
                <tbody>
                    @isset($items)
                        @foreach($items as $i)
                    <tr>
                            <td class="text-center">

                                    @if($i->confirm)
                                    <button type="button" class="btn btn-large btn-block btn-success" disabled>
                                            <i class="fas fa-check-double"></i>&nbsp;
                                            تم التسليم</button>
                                    @else 
                                    @if($i->active == 3)
                                    <button type="button" class="btn btn-large btn-block btn-danger" disabled>
                                            <i class="fas fa-ban"></i>&nbsp;
                                            تم الغاء الطلب</button>
                                    @else 
                                    <button type="button" class="btn btn-large btn-block btn-warning" disabled>
                                            <i class="far fa-clock"></i>&nbsp;
                                            قيد التسليم</button>
                                    @endif
    
                                    @endif
                                
    
                                
                            </td>
                            <td class="text-center" style="padding: 4px;">
                                    <nav class="navbar navbar-expand-sm">


                                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                           
                                            <li class="nav-item "  >
                                                @if($i->confirm)
                                                <form >
                                                            <button type="submit" class="btn btn-large  btn-default" disabled>
                                                                    <i class="fas fa-check-double"></i>
                                                             &nbsp;تمت الموافقة</button>
                                                    </form>
                                                @else 

                                                @if($i->active == 3)

                                                @else 
                                                <form action="{{route('confirmBooked')}}" method="post">
                                                        @csrf
                                                            <input type="hidden" name="id" value="{{$i->id}}">
                                                            <button type="submit" class="btn btn-large  btn-info">
                                                             <i class="fas fa-check-circle"></i>
                                                             &nbsp;موافقة</button>
                                                    </form>
                                                @endif

                                                @endif

                                            </li>
                                            &nbsp;
                                            &nbsp;
                                            &nbsp;
                                            <li class="nav-item "  >
                                                    @if($i->confirm)
                                                    @else 
                                                    @if($i->active != 3)
                                                    <form action="{{route('bookedCancelOwner')}}" method="post">
                                                            @csrf
                                                                <input type="hidden" name="id" value="{{$i->id}}">
                                                                    <button type="submit" class="btn btn-large  btn-danger">
                                                                            <i class="fas fa-trash"></i>&nbsp;
                                                                            الغاء 
                                                                        </button>
                                                            </form>
                                                            @endif
                                                    @endif

                                            </li>
                                    </ul>
                                            </div>
                                    </nav>
    
                                </td>
    

                    <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold"> من ساعة {{$i->from_hour}} الى ساعة {{$i->to_hour}}</td>
                    <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold"> من {{$i->from_date}} الى {{$i->to_date}}</td>
    
                      <!-- <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold">{{$i->deduction}}</td> -->
                        <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold">${{$i->price}}</td>
    
                        <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold">{{$i->number}}</td>
                        <td class="text-center">{{$i->name}}</td>
                        <td class="text-center">
                                
                                {{-- <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(70)->generate($i->serial_number))!!} "> --}}
                                <img src="{!! QrCode::format('png')->size(70)->generate($i->serial_number)!!} ">

                            
                        </td>
                        
                    </tr>
                    
                    @endforeach
                    @endisset
                </tbody>
            </table>
        </div>
        
    </div>
    
    
@stop