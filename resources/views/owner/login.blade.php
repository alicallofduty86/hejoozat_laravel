<!doctype html>
<html >
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Hejoozat</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Cairo:600');
            .content {
                text-align: center;
            }
            .title {
                font-size: 84px;
            }
            }
            .navbar-collapse{
                color: #ffe01a !important;
            }

                .btn-primary {
                background: #FFDD00;
                color: #fff;
                border: 1px solid #FFDD00; }
                .btn-primary:hover, .btn-primary:focus, .btn-primary:active {
                background: #ffe01a !important;
                border-color: #ffe01a !important;
                color: #2C2E3E !important; }
                .btn-primary.btn-outline {
                background: transparent;
                color: #FFDD00;
                border: 1px solid #FFDD00; }
                .btn-primary.btn-outline:hover, .btn-primary.btn-outline:focus, .btn-primary.btn-outline:active {
                    background: #FFDD00;
                    color: #fff; }
            * {

                font-family: 'Cairo', sans-serif;
            }
            body { background-color: #f9f4f4;;
                   color: #636b6f;
                   margin: 0;
                   padding-bottom: 100px;
                   min-height:967px;
                   position:relative;
                 }
                 .text-muted{
                    color: #28a745 !important;
                    font-family: arabic1;

                }

                .rate{
                    color: #f1c40f;
                }
                .full {
    width: 100%;	
}

.footer {
	height: auto;
	
    position: absolute;
    left: 0;
    bottom: 0;
    color:#fff;
	border-bottom: 1px solid #CCCCCC;
	border-top: 1px solid #DDDDDD;
    background: #00003E; 
    background: -webkit-linear-gradient(to right, #31BDE6, #00003E);  
    background: linear-gradient(to right, #31BDE6, #00003E);     
}

.footer-bottom {
    background: #40E0D0;
    background: -webkit-linear-gradient(to right, #FF0080, #FF8C00, #40E0D0);
    background: linear-gradient(to right, #FF0080, #FF8C00, #40E0D0);
	border-top: 1px solid #DDDDDD;
	padding-top: 20px;
	padding-bottom: 20px;
    color:#fff;
}
p{
    font-family: 'Cairo', sans-serif !important;
}
.colorgraph {
    height: 5px;
    border-top: 0;
    background: #c4e17f;
    border-radius: 5px;
    background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
    background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
    background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
    background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  }
</style>
<div class="container " style="margin-left: 33%;margin-top:10%;" >
     <div class="alert alert-success " role="alert" style="margin-left: 0;margin-top: 7%;width: 48%;">
       <h4 class="alert-heading text-right" style="text-align: center !important;">أهلا بك في تطبيق حجوزات </h4>
     </div>

        <div class="row" style="margin-top:20px">
            <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
            <div  style="outline: 1px solid orange;padding: 20px;box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);background: #ecf0f1;">
            <form method="POST" action="{{ route('ownertLogin') }}">
              @csrf
              <fieldset>
                
                <h2 style="margin-left:33%;">تسجيل دخول</h2>
                <hr class="colorgraph">
                <div class="form-group">
                            <input type="email" name="email" id="email" class="form-control input-lg text-right {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="ألأيميل">
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                </div>
                <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control input-lg text-right {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="كلمة المرور">
                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                </div>
                <span class="button-checkbox" dir="rtl" style="margin-left: 80%;">
                  
                    <label class="form-check-label" for="remember">

                        تذكرني
                        &nbsp;
                        &nbsp;
                    </label>
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                  <!--<a href="" class="btn btn-link pull-right">نسيت كلمة المرور ؟</a>-->
                </span>
                <hr class="colorgraph">
                <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12">
                                <button type="submit" class="btn btn-lg btn-success btn-block" >
                                <i class="fas fa-sign-in-alt"></i>&nbsp;&nbsp;&nbsp;تسجيل دخول</button>
                  </div>
                </div>
              </fieldset>
            </form>
            </div>
          </div>
        </div>
        
        </div>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
               
        <script>
                @if(Session::has('Title'))
                toastr["error"]("تأكد من الايميل و كلمة المرور", "مشكلة في تسجيل الدخول")
        
                toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                }
                console.log('dfgggggggdfmkngdfnglkdfngdflkglkgkldfjgklfdjgkfdl0000000000000000')
                @endif
        
                @if(Session::has('success'))
                toastr["success"]("{{Session::get('success')}}", "تمت العملية بنجاح")
        
                toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                }
                @endif
        
              </script>
      