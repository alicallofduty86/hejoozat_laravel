
@extends('layouts\layout')

@section('content')

<style>
    .yl{
        background:rgb(219, 253, 151);
        padding: 4px;
        border-radius: 5px;
        border: 1px solid #c1c2c2;
    }
    </style>
<!-- Page Content -->
<div class="container" >

    <!-- Portfolio Item Heading -->
    @foreach ($items as $i)
        
    
    <h1 class="my-4 yl">
      {{$i->name}}
    </h1>
  
    <!-- Portfolio Item Row -->
    <div class="row">
      <div class="col-md-8">
                      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            @if(isset($i->images[2]))
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            @endif
                            @if(isset($i->images[3]))
                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            @endif
                          </ol>
                          <div class="carousel-inner">
                                    <div  class="carousel-item active">
                                          <img class="d-block w-100 " style="height:410px;" src="../{{$i->images[0]->image_path}}">
                                    </div>
                                    <div class="carousel-item">
                                      <img class="d-block w-100 " style="height:410px;" src="../{{$i->images[1]->image_path}}" alt="Second slide">
                                    </div>
                                    @if(isset($i->images[2]))
                                    <div class="carousel-item">
                                      <img class="d-block w-100 " style="height:410px;" src="../{{$i->images[2]->image_path}}" alt="Third slide">
                                    </div>
                                    @endif
                                    @if(isset($i->images[3]))
                                    <div class="carousel-item">
                                      <img class="d-block w-100 " style="height:410px;" src="../{{$i->images[3]->image_path}}" alt="Third slide">
                                    </div>
                                    @endif
                            </div>
                          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
      </div>
  
      <div class="col-md-4 text-right">
        <h3 class="my-3 yl">...  الوصف</h3>
        <p>
          {{$i->disc}}
        </p>
        <h3 class="my-3 yl">...  المواصفات</h3>
        <ul class="float-right text-right" dir="rtl">
          <li>حجم الشاشة : 15.6 بوصة </li>
          <li>معالج : Intel Core i5‎-8250U </li>
          <li>معالج رسومي : AMD Radeon 530M .</li>
          <li>ذاكرة عشوائية : 16 جيجا بايت DDR4‏</li>
        </ul>
        <a href="" class="btn btn-success btn-block float-right" style="color:aliceblue"><i class="fas fa-cart-arrow-down"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;حجز </a>
      </div>
      
  
    </div>
    <!-- /.row -->
  
    <!-- Related Projects Row -->
    <h3 class="my-4 yl">منتجات أخرى</h3>
  
    <div class="row">
  
      <div class="col-md-3 col-sm-6 mb-4">
        <a href="#">
              <img class="img-fluid" src="http://placehold.it/500x300" alt="">
            </a>
      </div>
  
      <div class="col-md-3 col-sm-6 mb-4">
        <a href="#">
              <img class="img-fluid" src="http://placehold.it/500x300" alt="">
            </a>
      </div>
  
      <div class="col-md-3 col-sm-6 mb-4">
        <a href="#">
              <img class="img-fluid" src="http://placehold.it/500x300" alt="">
            </a>
      </div>
  
      <div class="col-md-3 col-sm-6 mb-4">
        <a href="#">
              <img class="img-fluid" src="http://placehold.it/500x300" alt="">
            </a>
      </div>
  
    </div>
    <!-- /.row -->
    @endforeach
  </div>
  <!-- /.container -->

  @stop