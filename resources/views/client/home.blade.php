@extends('layouts\layout')

@section('content')
<link href="{{ asset('css/search.css') }}" rel="stylesheet">
<div >
    
    <div class="row">
      <div id="carouselExampleIndicators5" class="carousel slide" data-ride="carousel" style="width:100%;">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators5" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators5" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators5" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div  class="carousel-item active">
                <img class="d-block w-100" style="height:400px;" src="https://www.rwsentosa.com/-/media/project/non-gaming/rwsentosa/hotels/hard-rock-hotel-singapore/hardrockhotelsg-exterior.jpg">
                <div class="carousel-caption d-none d-md-block">
                    <h5>حجوزات فندقية</h5>
                    <p>سارع الان و احجز</p>
                  </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" style="height:400px;" src="http://www.dealnloot.com/wp-content/uploads/2015/04/Book-My-Show-Banner.jpg" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
                <h5>حجوزات تذكر السينما</h5>
                <p>سارع الان و احجز فلمك المفضل الان</p>
              </div>
          </div>
          <div class="carousel-item">
            <img class="d-block w-100" style="height:400px;" src="http://mawaeedonline.com/wp-content/uploads/2016/03/landing_En-1536x600.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
                <h5>حجوزات طبية</h5>
                <p>سارع و احجز الان عند طبيبك المختص</p>
              </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators5" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators5" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
    
    <div >
        <br>
        <br>
  
      <div id="colorlib-reservation1">
        <!-- <div class="container"> -->
          <div class="row">
            <div class="search-wrap1">
              <div class="tab-content1" dir="rtl">
                <div id="" class="">
                  <form  class="colorlib-form1" method="POST" action="{{ route('search') }}">
                    @csrf
                          <div class="row">
                              <div class="col-md-2">
                                  <button  class="btn btn-primary btn-block">
                                    بحث
                                    &nbsp;
                                    &nbsp;
                                      <i class="fas fa-search"></i>
                                    </button>
                                </div>
                           <div class="col-md-4">
                             <div class="form-group">
                              <label for="date"></label>
                              <div class="form-field1">
                                <input type="text" id="location" style="height: 48px;" name="disc" class="form-control" placeholder="نص البحث"  >
                              </div>
                            </div>
                           </div>
                          <div class="col-md-2">
                            <div class="form-group">
                              <label for="guests"></label>
                              <div class="form-field1">
                                <i class="icon icon-arrow-down3"></i>
                                <select name="province" id="people" class="form-control"   style="height: 48px;">
                                  <option value="0">المحافظة</option>
                                  <option  value="1">بغداد</option>
                                  <option  value="2">كربلاء</option>
                                  <option  value="3">بابل</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2">
                              <div class="form-group">
                                <label for="guests"></label>
                                <div class="form-field1">
                                  <i class="icon icon-arrow-down3"></i>
                                  <select name="people" id="people" class="form-control" style="height: 48px;">
                                    <option value="0">التصنيف الرئيسي</option>
                                    @foreach($cat as $c)
                                    
                                    <option  value="{{$c->id}}">{{$c->name}}</option>
                                    @endforeach

                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                  <label for="guests"></label>
                                  <div class="form-field1" >
                                    <i class="icon icon-arrow-down3"></i>
                                    <select name="subCategory" id="people" class="form-control"  required style="height: 48px;">
                                      <option value="0">التصنيف الفرعي</option>
                                      <option  value="1">سويت</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                        </div>
                      </form>
                   </div>
       
                 </div>
            </div>
          </div>
        </div>
        
    </div>
    <br>
    <div class="container" >
      

    <nav aria-label="Page navigation example" style="width: 20%;margin-left: 46%;">
      {{ $item->links() }}
      </nav>
    </div>
    <div class="container">
      
  <div class="index-content">
    
    <div class="container">
        @foreach($item->chunk(3) as $chunk)
            <div class="row" style="padding:10px"> 
              
                @foreach($chunk as $i)
                <div class="col-lg-4">
                    <div class="card text-center" style="box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);background: #ecf0f1;font-weight: bold;">
                       
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                          <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            @if(isset($i->images[2]))
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            @endif
                            @if(isset($i->images[3]))
                            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                            @endif
                          </ol>
                          <div class="carousel-inner">
                                    <div  class="carousel-item active">
                                          <img class="d-block w-100 " style="height:200px;" src="../{{$i->images[0]->image_path}}">
                                    </div>
                                    <div class="carousel-item">
                                      <img class="d-block w-100 " style="height:200px;" src="../{{$i->images[1]->image_path}}" alt="Second slide">
                                    </div>
                                    @if(isset($i->images[2]))
                                    <div class="carousel-item">
                                      <img class="d-block w-100 " style="height:200px;" src="../{{$i->images[2]->image_path}}" alt="Third slide">
                                    </div>
                                    @endif
                                    @if(isset($i->images[3]))
                                    <div class="carousel-item">
                                      <img class="d-block w-100 " style="height:200px;" src="../{{$i->images[3]->image_path}}" alt="Third slide">
                                    </div>
                                    @endif
                            </div>
                          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                          </a>
                          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </div>
                        <div class="card-body">
                          <h5 class="card-title">{{$i->name}}</h5>
                          <p class="card-text">
                              <i class="fas fa-star rate  "></i>
                              <i class="fas fa-star rate  "></i>
                              <i class="fas fa-star rate  "></i>
                              <i class="fas fa-star-half-alt rate"></i>
                              <i class="far fa-star rate"></i>
                          </p>
                          <p class="card-text">{{$i->owner->name}}</p>
                          <p class="card-text"><span class="text-muted m-r-5" style="">${{$i->item_price->price}}</span></p>
                        </div>
                        <div class="card-footer text-muted">
                            <nav class="navbar navbar-expand-sm">


                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav ml-auto">

                            <li class="nav-item "  >
                                <form action="{{route('getById')}}" method="post">
                                @csrf
                              <button type="submit" class="btn btn-info " style="color:aliceblue"><i class="fas fa-info-circle"></i>&nbsp;&nbsp;تفاصيل </button>
                              <input type="hidden" name="id" value="{{$i->id}}">
                              &nbsp;
                              &nbsp;
                            </form>
                            </li>
                            <li class="nav-item "  >
                            
                                <form>
                             
                             <button type="button" class="btn btn-success" style="color:aliceblue" data-toggle="modal" data-target="#myModal"><i class="fas fa-cart-arrow-down"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;حجز </button>
                            </form>
                            </li>
                            </ul>
                            </div>
                            </nav>

                             <form action="{{route('booking')}}" method="post">
                                @csrf
                              <input type="hidden" name="item_id" value="{{$i->id}}">
                             <!-- The Modal -->
                            
                                <div class="modal fade" id="myModal" >
                                  <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content" style="padding:15px">
                                      <div class="alert alert-success" role="alert">
                                        <h4 class="alert-heading"> املاء البيانات</h4>
                                      </div>
                                    
                                     <div class="row">
                                       <div class="col-md-6">
                                          <div class="input-group mb-3">
                                              <input type="date" name="to_date" class="form-control text-right" placeholder="التاريخ" aria-label="" aria-describedby="basic-addon2" >
                                              <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">الى</span>
                                              </div>
                                            </div>
                                       </div>
                                       <div class="col-md-6">
                                          <div class="input-group mb-3">
                                              <input type="date" name="from_date" class="form-control text-right" placeholder="التاريخ" aria-label="" aria-describedby="basic-addon2" >
                                              <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">من</span>
                                              </div>
                                            </div>
                                        </div>
                                     </div>
                                
                                     <div class="row">
                                        <div class="col-md-6">
                                           <div class="input-group mb-3">
                                               <input type="number" name="to_hour" class="form-control text-right" placeholder="الساعة" aria-label="" aria-describedby="basic-addon2" >
                                               <div class="input-group-append">
                                                 <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">الى</span>
                                               </div>
                                             </div>
                                        </div>
                                        <div class="col-md-6">
                                           <div class="input-group mb-3">
                                               <input type="number" name="from_hour" class="form-control text-right" placeholder="الساعة" aria-label="" aria-describedby="basic-addon2" >
                                               <div class="input-group-append">
                                                 <span class="input-group-text" id="basic-addon2" style="background:rgb(219, 253, 151)">من</span>
                                               </div>
                                             </div>
                                         </div>
                                      </div>
                    
                                      <div class="row">
                                         <div class="col-md-6" style="padding-right: 30px;padding-left: 10px;">
                                            <div class="form-check">
              
                                                 <div class="row" >
                                                       <div class=" col-lg-9 col-xs-9 col-sm-9 col-md-9 text-right">
                                                          <label class="form-check-label" for="defaultCheck1">
                                                          حجز مسائي
                                                          </label>
                                                       </div>
                                                        <div class=" col-lg-3 col-xs-3 col-sm-3 col-md-3">
                                                         <input class="form-check-input" name="am" value="0"  type="radio" id="defaultCheck222">
                                                        </div>
                                                
                                                      </div>
                                              </div>  
                                         </div>
                                         <div class="col-md-6" style="padding-right: 30px;padding-left: 10px;">
                                            <div class="form-check">
              
                                                <div class="row" >
                                                    <div class=" col-lg-9 col-xs-9 col-sm-9 col-md-9 text-right">
                                                        <label class="form-check-label" for="defaultCheck1">
                                                          حجز صباحي
                                                          </label>
                                                      </div>
                                                  <div class=" col-lg-3 col-xs-3 col-sm-3 col-md-3">
                                                      <input class="form-check-input" name="am"  type="radio" value="1" id="defaultCheck222">
                                                  </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                        <hr style="background: #777 !importan">
                                       <div class="row">
                                          <div class="col-md-6">
                                            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">
                                                <i class="fas fa-window-close"></i>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                              الغاء
                                            </button>
                                          </div>
                                          <div class="col-md-6">
                                            <button type="submit" class="btn btn-success btn-block">
                                                <i class="fas fa-cart-arrow-down"></i>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                              حجز
                                            </button>
                                            </div>
                                       </div>
                                    </div>
                                  </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
                @endforeach



                
  
 
</div>


  
    
    
</div>
@stop
