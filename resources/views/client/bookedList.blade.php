@extends('layouts\layout')

@section('content')

<br>
<div class="container" >

   
        <div class="table-responsive" style="background: #ecf0f1;">
            <table class="table table-bordered">
                <thead class="" style="background-color: #343a40!important;color: aliceblue">
                    <tr>
                        <th class="text-center" >حالة الحجز</th>
                        <th class="text-center" >-----</th>
    

                        <th class="text-center">وقت الحجز</th>
                        <th class="text-center">تاريخ الحجز</th>
    
                       <!-- <th class="text-center">المبلغ الدفوع</th> -->
    
                        <th class="text-center">السعر</th>
        
                        <th class="text-center">العدد</th>
           
                        <th class="text-center">المادة</th>
                        <th class="text-center">Order_QR</th>
                    </tr>
                </thead>
                <tbody>
                    @isset($items)
                        @foreach($items as $i)
                    <tr>
                            <td class="text-center">
                                    <nav class="navbar navbar-expand-sm">
                                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                    @if($i->confirm)
                                    <li class="nav-item "  >
                                    <form>
                                    <button type="button" class="btn  btn-success" disabled>
                                        <i class="fas fa-check-circle"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        تم التسليم</button>
                                    </form>
                                    </li>
                                    @else
                                        @if($i->active == 3)
                                        <li class="nav-item "  >
                                        <form>
                                        <button type="button" name="" id="" class="btn btn-danger" disabled>
                                                <i class="fas fa-ban"></i>&nbsp;
                                                تم الالغاء من الادارة
                                            </button>
                                        </form>
                                        </li>
                                        @else 
                                        <li class="nav-item "  >
                                        <form>
                                        <button type="button" name="" id="" class="btn btn-warning" >
                                                <i class="fa fa-backward" aria-hidden="true"></i>&nbsp;
                                                قيد التسليم
                                            </button>
                                        </form>
                                        </li>
                                        @endif

                                    @endif
                                    </ul>
                                    </div>
                                    </nav>
                                    
                                </td>
                            <td class="text-center">
                                    <nav class="navbar navbar-expand-sm">


                                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                            @if($i->confirm)
                                            <li class="nav-item "  >
                                                    <form >
                                                            <button type="submit" class="btn btn-large btn-block  btn-default" disabled>
                                                                    <i class="fas fa-check-double"></i>
                                                             &nbsp;تمت الموافقة</button>
                                                    </form>
                                                    </li>
                                            @else 
                                            <li class="nav-item "  >
                                                    @if($i->active == 3)
                                                    @else 
                                                    <form action="" method="post">
                                                            <button type="button" class="btn btn-large  btn-warning">
                                                             <i class="fas fa-pen-square"></i>
                                                             &nbsp;تعديل</button>
                                                    </form>
                                                    </li>
                                                    &nbsp;
                                                    &nbsp;
                                                    &nbsp;
                                                    <li class="nav-item "  >
                                                            <form action="{{route('bookedCancel')}}" method="post">
                                                            @csrf
                                                                <input type="hidden" name="id" value="{{$i->id}}">
                                                                    <button type="submit" class="btn btn-large  btn-danger">
                                                                            <i class="fas fa-trash"></i>&nbsp;
                                                                            الغاء 
                                                                        </button>
                                                            </form>
                                                    </li> 
                                                    @endif
                                            @endif

                                    </ul>
                                            </div>
                                    </nav>
    
                                </td>
    

    
                        <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold"> من ساعة {{$i->from_hour}} الى ساعة {{$i->to_hour}}</td>
                        <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold"> من {{$i->from_date}} الى {{$i->to_date}}</td>

                      <!-- <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold">{{$i->deduction}}</td> -->
                        <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold">${{$i->price}}</td>
    
                        <td class="text-center" style="font-family:'Times New Roman', Times, serif;font-weight: bold">{{$i->number}}</td>
                        <td class="text-center">{{$i->name}}</td>
                        <td class="text-center"><img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(70)->generate($i->serial_number))!!} "></td>
                    </tr>
                    @endforeach
                    @endisset
                </tbody>
            </table>
        </div>
        
    </div>
    
    
@stop