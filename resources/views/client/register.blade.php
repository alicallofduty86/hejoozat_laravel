@extends('layouts\layout')

@section('content')

<div class="container " >

    <div class="row" style="margin-top:20px">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 center" style="margin-left: 23%;">
        <div  style="outline: 1px solid orange;padding: 20px;border-radius: 20%;border-radius: 30px;">
          <fieldset>
            <form >
            <h2 style="margin-left:37%;">أنشاء حساب</h2>
            <hr class="colorgraph">
            <div class="form-group">
                <input type="text" name="name" id="name" class="form-control text-right"  placeholder="ألاسم" required>
           </div>
            <div class="form-group">
                        <input type="email" name="email" id="email" class="form-control  text-right"  placeholder="ألأيميل" required email>
            </div>
            <div class="form-group">
                <input type="email" name="phone" id="phone" class="form-control  text-right"  placeholder="الهاتف" required>
             </div>
            <div class="row">
              <div class="col-md-6">
                  <img  id="imageid" src="../assets/img/3.png" alt=""  style="width: 40px;height: 40px;float:right;" required>
                  <img  name="image" id="image" src="../assets/img/3.png" alt="" style="width: 100%;height: 100%;display: none" >
              </div>
              <div class="col-md-6">
                  <div class="form-group" dir="rtl">
                      <label class="btn btn-outline-secondary btn-block btn-file">
                          حمل الصورة الشخصية <input type="file" style="display: none;">
                      </label>
                      
                    </div>
              </div>
            </div>

              <div class="form-group ">
                  <select class="form-control form-control-lg text-right" name="contry"  dir="rtl" required>
                      <option>الدولة</option>
                      <option value="1">العراق</option>
                    </select>
              </div>
              <div class="form-group ">
                  <select class="form-control form-control-lg text-right" name="province"  dir="rtl" required>
                      <option>المحافظة</option>
                      <option value="1">بغداد</option>
                    </select>
              </div>
            <div class="form-group">
                        <input type="password" name="password" id="password" class="form-control  text-right"  placeholder="كلمة المرور" required>
            </div>
   
            <hr class="colorgraph">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12">
                            <button type="submit" class="btn btn-lg btn-outline-success btn-block"  ><i class="fas fa-user-plus"></i>&nbsp;&nbsp;تسجيل 
                 </button>
              </div>
            </div>
          </form>
          </fieldset>
        </div>
      </div>
    </div>
    
    </div>
  

@stop