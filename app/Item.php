<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Item extends Model
{

    protected $table = 'items';

    protected $primaryKey = 'id';

    protected $fillable = ['name','votos','number','disc','availability','status',
                            'max_requests','sub_category_id','owner_id'];
    
    public function sub_category()
    {
        return $this->belongsTo('App\SubCategory');
    }

    public function owner()
    {
        return $this->belongsTo('App\Owner');
    }

    public function hotel_park()
    {
        return $this->hasOne('App\HotelPark');
    }

    public function item_price()
    {
        return $this->hasOne('App\ItemPrice');
    }
    public function item_rate()
    {
        return $this->hasOne('App\ItemRate');
    }
     public function flight()
     {
         return $this->hasOne('App\Flight');
     }

     public function device()
     {
         return $this->hasOne('App\Device');
     }

     public function cinema()
     {
         return $this->hasOne('App\Cineme');
     }

     public function images()
     {
         return $this->hasMany('App\Image');
     }

     public function medical()
     {
         return $this->hasOne('App\Medical');
     }

     public function reservation()
     {
         return $this->hasOne('App\Reservation');
     }

     public function start_end()
     {
         return $this->hasOne('App\StartEnd');
     }

     public function time_to_open(){
         return $this->hasOne('App\TimeToOpen');
     }

     public function tourist_tour()
     {
         return $this->hasOne('App\TouristTour');
     }



}