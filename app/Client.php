<?php

namespace App;

use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;


class Client extends Authenticatable
{
    use Notifiable;

    protected $guard  = 'client';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','phone','address_id','status','token','img_path'
    ];

    protected $hidden = [
        'password','email', 'remember_token',
    ];
   
    public function address()
    {
        return $this->belongsTo('App\Address');
    }

}