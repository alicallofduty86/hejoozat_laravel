<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Medical extends Model 
{

    protected $table = 'medical';

    protected $primaryKey = 'id';

    protected $fillable = [
        'item_id',
        'educational_attainment',
        'worktime',
        'votes',
        'time_to_open_id',
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function time_to_open()
    {
        return $this->belongsTo('App\TimeToOpen');
    }

}