<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TouristTour extends Model 
{

    protected $table = 'tourist_tour';

    protected $primaryKey = 'id';

    protected $fillable = [
        'item_id',
        'hotel_rate',
        'duration',
        'date_of_tour',
        'free_children',
        'breakfast_included',
        'dinner_included',
        'launch_included',
        'open_food',
        'time_to_open_id',
        'to_country_id',
        'to_province_id',
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function time_to_open()
    {
        return $this->belongsTo('App\TimeToOpen');
    }

    public function to_country()
    {
        return $this->belongsTo('App\Country');
    }

    public function to_province()
    {
        return $this->belongsTo('App\Province');
    }

}