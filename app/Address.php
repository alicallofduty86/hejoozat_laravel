<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Address extends Model 
{

    protected $table = 'address';

    protected $primaryKey = 'id';

    protected $fillable = ['country_id','province_id','long','lat','discraption'];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function province()
    {
        return $this->belongsTo('App\Province');
    }


}