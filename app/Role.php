<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Role extends Model
{

    protected $table = 'Levels';

    protected $primaryKey = 'id';

    protected $fillable = ['role'];
   

}