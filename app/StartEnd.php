<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class StartEnd extends Model 
{


    protected $table = 'start_end';

    protected $primaryKey = 'id';

    protected $fillable = ['start_date','end_date','item_id','status'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }


}