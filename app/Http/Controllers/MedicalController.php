<?php
namespace App\Http\Controllers;
use Validator;
use App\Item;
use App\Image;
use App\Medical;
use App\ItemPrice;
use App\TimeToOpen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller as BaseController;
class MedicalController extends BaseController 
{
    public function store(request $request){
        $Data = null;
       // dd($request->all());
        DB::beginTransaction();
        try{
            $Item = new Item;
            $Item->name = $request->input('name');
            $Item->number = 1;//$request->input('number');
            $Item->max_requests = $request->input('max_requests');
            $Item->sub_category_id = 1;//$request->input('sub_category_id');
            $Item->disc = $request->input('disc');
            $Item->owner_id = 1;//Auth::guard('web')->user()->id;
            $Item->save();
            $Item_id = $Item->id;
            if ($request->hasFile('image1')) {
                $image1 = $request->file('image1');
                $image1NewName = uniqid().(int)microtime(1).$image1->getClientOriginalName();
                $image1->move('uploads',$image1NewName);
                $Image = new Image;
                $Image->item_id = $Item_id;
                $Image->image_path = 'uploads/' . $image1NewName;
                $Image->save();
            }
            if ($request->hasFile('image2')) {
                $image2 = $request->file('image2');
                $image2NewName = uniqid().(int)microtime(1).$image2->getClientOriginalName();
                $image2->move('uploads',$image2NewName);
                $Image = new Image;
                $Image->item_id = $Item_id;
                $Image->image_path = 'uploads/' . $image2NewName;
                $Image->save();
            }
            if ($request->hasFile('image3')) {
                $image3 = $request->file('image3');
                $image3NewName = uniqid().(int)microtime(1).$image3->getClientOriginalName();
                $image3->move('uploads',$image3NewName);
                $Image = new Image;
                $Image->item_id = $Item_id;
                $Image->image_path = 'uploads/' . $image3NewName;
                $Image->save();
            }
            if ($request->hasFile('image4')) {
                $image4 = $request->file('image4');
                $image4NewName = uniqid().(int)microtime(1).$image4->getClientOriginalName();
                $image4->move('uploads',$image4NewName);
                $Image = new Image;
                $Image->item_id = $Item_id;
                $Image->image_path = 'uploads/' . $image4NewName;
                $Image->save();
            }
            $Price = new ItemPrice;
            $Price->item_id = $Item_id;
            $Price->price = $request['price'];
            $Price->book_without_credit_card = (($request['withoutCredit'] == null || $request['withoutCredit'] == false) ? false :true);
            $Price->free_cancellation = (($request['freeCancellation'] == null || $request['freeCancellation'] == false) ? false :true);
            $Price->deduction = $request['deduction'];
            $Price->no_prepayment = (($request['noPrepayment'] == null || $request['noPrepayment'] == false) ? false :true);
            $Price->cancellation_deduction_ratio = (($request['cancellationDeduction'] == null || $request['cancellationDeduction'] == false) ? false :true);
            $Price->save();
            $time = new TimeToOpen;
            $time->from_day = $request->input('from_day');
            $time->to_day = $request->input('to_day');
            $time->from_time = $request->input('from_time');
            $time->to_time = $request->input('to_time');
            $time->every_day = 1;//$request->input('every_day');
            $time->every_time = 1;//$request->input('every_time');
            $time->save();
            $Medical= new Medical;
            $Medical->item_id = $Item_id;
            $Medical->educational_attainment = $request->input('educational_attainment');
            $Medical->votes = 0;//$request->input('votes');
            $Medical->worktime = 0;//$request->input('worktime');
            $Medical->time_to_open_id = $time->id;
            $Medical->save();
            $Data = $Medical->id;
        }catch(\Illuminate\Database\QueryException $e){
            dd($e->errorInfo);
            DB::rollback();
            $Data = null;
        }
        DB::commit();
        if(empty($Data)){
            return redirect()->back()->with('error','لم يتم حفظ المعلومات');
        }else{
            return redirect()->back()->with('success','تم حفظ المعلومات بنجاح');

        }

        return view('home');
    }

    public function getAll()
    {
        $Item = Item
        ::join('item_price','items.id',"=","item_price.item_id")
        ->join("medical",'items.id',"=","medical.item_id")
        ->join("time_to_open",'medical.time_to_open_id',"=","time_to_open.id")
        ->where("items.status","=",1)
        ->getQuery()
        ->get();
        return response()->json([
            'success' => true,
            'Data' => $Item
        ], 201);
    }


    public function getBy($id)
    {
        $Item = Item
        ::join('item_price','items.id',"=","item_price.item_id")
        ->join("medical",'items.id',"=","medical.item_id")
        ->join("time_to_open",'medical.time_to_open_id',"=","time_to_open.id")
        ->where("items.id","=",$id)
        ->getQuery()
        ->get();
        return response()->json([
            'success' => true,
            'Data' => $Item
        ], 201);
    }
}

