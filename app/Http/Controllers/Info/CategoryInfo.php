<?php

namespace App\Http\Controllers\Info;
use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use Laravel\Lumen\Routing\Controller as BaseController;

class CategoryInfo extends BaseController
{
    public function getCategory(){
     $category = new Category;
     $all = $category->all();
     return redirect()->back()->withInput('category',$all);
    }

    public function getSubCategory($id){
        $subCategory = SubCategory::where('category_id','=',$id)->get();
        return redirect()->back()->withInput('subCat',$subCategory);
    }
}
