<?php

namespace App\Http\Controllers\Info;
use App\Brand;
use Validator;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class BrandController extends BaseController
{
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'path' => 'required',
            ]);
        $com = new Brand();
        $com->name = $request->input('name');
        $com->path = $request->input('path');
        $com->save();
        return response()->json([
            'success' => true,
            'Data' => 'Data Inserted'
        ], 201);
    }

    public function edit(Request $request,$id){
        $com = Brand::find($id);
        $com->name = $request->input('name');
        $com->path = $request->input('path');
        $com->save();
        return response()->json([
            'success' => true,
            'Data' => 'Data updated'
        ], 201);
    }
    public function getAll(){
        $com = Brand::all();
        return response()->json([
            'success' => true,
            'Data' => $com
        ], 201);
    }
}