<?php

namespace App\Http\Controllers\Info;
use App\Brand;
use App\Country;
use App\Province;
use App\Address;
use Validator;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class AddressController extends BaseController
{

    public function storeAddress(Request $request){
        $this->validate($request, [
            'country_id' => 'required|integer',
            'province_id' => 'required|integer',
            'long' => 'required',
            'lat' => 'required',
            'discraption' => 'required'
            ]);
            try{
                $Address = new Address();
                $Address->country_id = $request->input('country_id');
                $Address->province_id = $request->input('province_id');
                $Address->long = $request->input('long');
                $Address->lat = $request->input('lat');
                $Address->discraption = $request->input('discraption');
                $Address->save();
                return response()->json([
                    'success' => true,
                    'Data' => 'Data Inserted'
                ], 201);
            }catch(\Illuminate\Database\QueryException $e){
                return response()->json([
                    'success' => false,
                    'Data' => 'Data did ont Inserted'
                ], 433);
            }

    }

    public function editAddress(Request $request){
        $this->validate($request, [
            'id' => 'required|integer',
            'country_id' => 'required|integer',
            'province_id' => 'required|integer',
            'long' => 'required',
            'lat' => 'required',
            'discraption' => 'required'
            ]);
        $Address =Address::find($request->input('id'));
        $Address->country_id = $request->input('country_id');
        $Address->province_id = $request->input('province_id');
        $Address->long = $request->input('long');
        $Address->lat = $request->input('lat');
        $Address->discraption = $request->input('discraption');
        $Address->save();
        return response()->json([
            'success' => true,
            'Data' => 'Data updated'
        ], 201);
    }

    public function getAddress($id){
        $Address =Address::find($id)->all();
        return response()->json([
            'success' => true,
            'Data' => $Address
        ], 201);
    }

    public function storeProvince(Request $request){
        $this->validate($request, [
            'country_id' => 'required|integer',
            'name' => 'required',
            
            ]);
        $Province = new Province();
        $Province->country_id = $request->input('country_id');
        $Province->name = $request->input('name');
        $Province->save();
        return response()->json([
            'success' => true,
            'Data' => 'Data Inserted'
        ], 201);
    }

    public function storeCountry(Request $request){
        $this->validate($request, [
            'name' => 'required'
            ]);
        $Country = new Country();
        $Country->name = $request->input('name');
        $Country->save();
        return response()->json([
            'success' => true,
            'Data' => 'Data Inserted'
        ], 201);
    }

    public function editProvince(Request $request){
        $this->validate($request, [
            'id' => 'required|integer',
            'country_id' => 'required|integer',
            'name' => 'required'
            ]);
        $Province = Province::find($request->input('id'));
        $Province->country_id = $request->input('country_id');
        $Province->name = $request->input('name');
        $Province->save();
        return response()->json([
            'success' => true,
            'Data' => 'Data updated'
        ], 201);
    }

    public function editCountry(Request $request){
        $Province = Province::find($request->input('id'));
        $Province->name = $request->input('name');
        $Province->save();
        return response()->json([
            'success' => true,
            'Data' => 'Data updated'
        ], 201);
    }

    public function getCountry(){
        $com = Country::all();
        return redirect()->back()->withInput('country',$com);
    }

    public function getProvince($id){
        $com = Province::find($id)->all();
        return redirect()->back()->withInput('province',$com);
    }
}