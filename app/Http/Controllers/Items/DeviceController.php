<?php
namespace App\Http\Controllers\Items;
use Validator;
use App\Item;
use App\Device;
use App\ItemPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller as BaseController;
class DeviceController extends BaseController 
{
    public function store(request $request){
        $this->validate($request, [
            'name' => 'required',
            'number' => 'required',
            'discraption' => 'required',
            'max_requests' => 'required',
            'sub_category_id' => 'required',
            'owner_id' => 'required',
            'price'=> 'required',
            'book_without_credit_card'=> 'required',
            'free_cancellation'=> 'required',
            'deduction'=> 'required',
            'no_prepayment'=> 'required',
            'cancellation_deduction_ratio'=> 'required',
            'color'=> 'required',
            'size'=> 'required',
            'smart'=> 'required',
            'threeD'=> 'required',
            'curve'=> 'required',
            'brand_id'=> 'required',
            ]);
        $Data = null;
        DB::beginTransaction();
        try{
            $Item = new Item;
            $Item->name = $request->input('name');
            $Item->number = $request->input('number');
            $Item->max_requests = $request->input('max_requests');
            $Item->sub_category_id = $request->input('sub_category_id');
            $Item->disc = $request->input('discraption');
            $Item->owner_id = $request->input('owner_id');
            $Item->save();
            $Item_id = $Item->id;
            $Price = new ItemPrice;
            $Price->item_id = $Item_id;
            $Price->price = $request->input('price');
            $Price->book_without_credit_card = $request->input('book_without_credit_card');
            $Price->free_cancellation = $request->input('free_cancellation');
            $Price->deduction = $request->input('deduction');
            $Price->no_prepayment = $request->input('no_prepayment');
            $Price->cancellation_deduction_ratio = $request->input('cancellation_deduction_ratio');
            $Price->save();
            $Device= new Device;
            $Device->item_id = $Item_id;
            $Device->color = $request->input('color');
            $Device->size = $request->input('size');
            $Device->smart = $request->input('smart');
            $Device->threeD = $request->input('threeD');
            $Device->curve = $request->input('curve');
            $Device->brand_id = $request->input('brand_id');
            $Device->save();
            $Data = $Device->id;
        }catch(\Illuminate\Database\QueryException $e){
            //var_dump($e->errorInfo);
            DB::rollback();
            $Data = null;
        }
        DB::commit();
        if(empty($Data)){
            return response()->json([
                'success' => false,
                'Data' => null
            ], 400);
        }else{
            return response()->json([
                'success' => true,
                'Data' => null
            ], 201);
        }
    }

    public function getAll()
    {
        $Item = Item
        ::join('item_price','items.id',"=","item_price.item_id")
        ->join("device",'items.id',"=","device.item_id")
        ->where("items.status","=",1)
        ->getQuery()
        ->get();
        return response()->json([
            'success' => true,
            'Data' => $Item
        ], 201);
    }


    public function getBy($id)
    {
        $Item = Item
        ::join('item_price','items.id',"=","item_price.item_id")
        ->join("device",'items.id',"=","device.item_id")
        ->where("items.id","=",$id)
        ->getQuery()
        ->get();
        return response()->json([
            'success' => true,
            'Data' => $Item
        ], 201);
    }
}

