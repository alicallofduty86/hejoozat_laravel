<?php
namespace App\Http\Controllers\Items;
use Validator;
use App\Images;
use App\Item;
use App\ItemPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Http\Controllers\Controller as BaseController;
class SearchController extends BaseController 
{
    public function getResult($disc,$provinceId,$id){
            $item = Item::with(['item_price','time_to_open','start_end','images','owner' => function($query) use($provinceId){
                $query->join('address','owner.address_id','=','address.id')
                ->where('province_id',$provinceId);
            } ])
            ->where('sub_category_id','=',$id)
            ->orWhere('disc','like','%'.$disc.'%')
            ->orWhere('name','like','%'.$disc.'%')
            ->get();
            $filtered = $item->where('owner','!=',null);
            $i = $filtered->all();
           // return $i;
            return response()->json([
                'success' => true,
                'Data' => $i
            ], 200);
            
    }

    public function getAll()
    {
        $item = Item::with('item_price','time_to_open','start_end','images')
        ->get();
        return response()->json([
            'success' => true,
            'Data' => $item
        ], 201);
    }


    public function getById(Request $request)
    {
        $item = Item::with('hotel_park','item_price','device','medical','flight','tourist_tour','images','start_end','time_to_open')
        ->where('id','=',$request->id)
        ->get();
        return view('client/details')->with('items',$item);
       // dd( $item);
    }

    public function getLast(){
        $item = Item::with('item_price','time_to_open','start_end','images')
        ->orderBy('created_at','desc')
        ->paginate(6);
        return redirect()->back()->with('last',$item);
    }
}