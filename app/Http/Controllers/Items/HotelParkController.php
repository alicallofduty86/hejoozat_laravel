<?php
namespace App\Http\Controllers\Items;
use Illuminate\Http\Request;
use App\Item;
use App\HotelPark;
use App\ItemPrice;
use App\Image;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
class HotelParkController extends Controller 
{
    public function store(Request $request){

        $Data = null;
        //dd($request['name']);
        DB::beginTransaction();
        try{
            $Item = new Item;
            $Item->name = $request['name'];
            $Item->number = $request['number'];
            $Item->max_requests = $request['maxRequest'];
            $Item->sub_category_id = 1 ;// $request['category'];
            $Item->disc = $request['disc'];
            $Item->owner_id = 1;// $request['owner_id'];
            $Item->save();
            $Item_id = $Item->id;
            if ($request->hasFile('image1')) {
                $image1 = $request->file('image1');
                $image1NewName = uniqid().(int)microtime(1).$image1->getClientOriginalName();
                $image1->move('uploads',$image1NewName);
                $Image = new Image;
                $Image->item_id = $Item_id;
                $Image->image_path = 'uploads/' . $image1NewName;
                $Image->save();
            }
            if ($request->hasFile('image2')) {
                $image2 = $request->file('image2');
                $image2NewName = uniqid().(int)microtime(1).$image2->getClientOriginalName();
                $image2->move('uploads',$image2NewName);
                $Image = new Image;
                $Image->item_id = $Item_id;
                $Image->image_path = 'uploads/' . $image2NewName;
                $Image->save();
            }
            if ($request->hasFile('image3')) {
                $image3 = $request->file('image3');
                $image3NewName = uniqid().(int)microtime(1).$image3->getClientOriginalName();
                $image3->move('uploads',$image3NewName);
                $Image = new Image;
                $Image->item_id = $Item_id;
                $Image->image_path = 'uploads/' . $image3NewName;
                $Image->save();
            }
            if ($request->hasFile('image4')) {
                $image4 = $request->file('image4');
                $image4NewName = uniqid().(int)microtime(1).$image4->getClientOriginalName();
                $image4->move('uploads',$image4NewName);
                $Image = new Image;
                $Image->item_id = $Item_id;
                $Image->image_path = 'uploads/' . $image4NewName;
                $Image->save();
            }
            $Price = new ItemPrice;
            $Price->item_id = $Item_id;
            $Price->price = $request['price'];
            $Price->book_without_credit_card = (($request['withoutCredit'] == null || $request['withoutCredit'] == false) ? false :true);
            $Price->free_cancellation = (($request['freeCancellation'] == null || $request['freeCancellation'] == false) ? false :true);
            $Price->deduction = $request['deduction'];
            $Price->no_prepayment = (($request['noPrepayment'] == null || $request['noPrepayment'] == false) ? false :true);
            $Price->cancellation_deduction_ratio = (($request['cancellationDeduction'] == null || $request['cancellationDeduction'] == false) ? false :true);
            $Price->save();
            $HotelPark= new HotelPark;
            $HotelPark->item_id = $Item_id;
            $HotelPark->adults = $request['adultNumber'];
            $HotelPark->childrenn = $request['childNumber'];
            $HotelPark->number_of_room = $request['roomNumber'];
            $HotelPark->family_room = (($request['familyRoom'] == null || $request['familyRoom'] == false) ? false :true);
            $HotelPark->double_bed = (($request['duoBed'] == null || $request['duoBed'] == false) ? false :true);
            $HotelPark->twin_beds = (($request['twoBed'] == null || $request['twoBed'] == false) ? false :true);
            $HotelPark->swimming_pool = (($request['swim'] == null || $request['swim'] == false) ? false :true);
            $HotelPark->sauna = (($request['sawna'] == null || $request['sawna'] == false) ? false :true);
            $HotelPark->breakfast_included = (($request['breakFast'] == null || $request['breakFast'] == false) ? false :true);
            $HotelPark->fitness_center = (($request['sportCenter'] == null || $request['sportCenter'] == false) ? false :true);
            $HotelPark->dinner_included = (($request['diner'] == null || $request['diner'] == false) ? false :true);
            $HotelPark->self_catering = (($request['openFood'] == null || $request['openFood'] == false) ? false :true);
            $HotelPark->free_wifi = (($request['freeWifi'] == null || $request['freeWifi'] == false) ? false :true);
            $HotelPark->parking = (($request['park'] == null || $request['park'] == false) ? false :true);
            $HotelPark->shuttle = (($request['transport'] == null || $request['transport'] == false) ? false :true);
            $HotelPark->private_bathroom = (($request['privateBathroom'] == null || $request['privateBathroom'] == false) ? false :true);
            $HotelPark->balcony = (($request['blkona'] == null || $request['blkona'] == false) ? false :true);
            $HotelPark->flat_screen_tv = (($request['tv'] == null || $request['tv'] == false) ? false :true);
            $HotelPark->air_conditioning = (($request['seblet'] == null || $request['seblet'] == false) ? false :true);
            $HotelPark->hairdryer = (($request['hireDray'] == null || $request['hireDray'] == false) ? false :true);
            $HotelPark->wellness_centre = $request['clinic'];(($request['clinic'] == null || $request['clinic'] == false) ? false :true);
            $HotelPark->upper_floors_accessible_by_lift =(($request['evelator'] == null || $request['evelator'] == false) ? false :true);
            $HotelPark->save();
            $Data = $HotelPark->id;
        }catch(\Illuminate\Database\QueryException $e){
            var_dump($e->errorInfo);
            DB::rollback();
            $Data = null;
        }
        DB::commit();
        if(empty($Data)){
            return redirect()->back()->with('error','لم يتم حفظ المعلومات');
        }else{
            return redirect()->back()->with('success','تم حفظ المعلومات بنجاح');

        }
    }

    public function getAll()
    {
        $Item = Item
        ::join('item_price','items.id',"=","item_price.item_id")
        ->join("hotel_park",'items.id',"=","hotel_park.item_id")
        ->where("items.status","=",1)
        ->getQuery()
        ->get();
        return response()->json([
            'success' => true,
            'Data' => $Item
        ], 201);
    }


    public function getBy($id)
    {
        $Item = Item
        ::join('item_price','items.id',"=","item_price.item_id")
        ->join("hotel_park",'items.id',"=","hotel_park.item_id")
        ->where("items.id","=",$id)
        ->getQuery()
        ->get();
        return response()->json([
            'success' => true,
            'Data' => $Item
        ], 201);
    }
}

