<?php
namespace App\Http\Controllers\Auth;
use Validator;
use App\Client;
use App\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Files\uploadImage;
use Laravel\Lumen\Routing\Controller as BaseController;
class ClientInfoController extends BaseController 
{
    public function signUp(request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'phone' => 'required',
            'province_id' => 'required',
            'country_id' => 'required',
            'token' => 'required',
            'mainFolder' => 'required',
            'folder' => 'required',
            'type' => 'required',
     
            ]);

        $Data = null;
        try{
            $upload = new uploadImage;
            $path = $upload->uploadInner($request);
            $address = new Address;
            $address->country_id = $request->country_id;
            $address->province_id = $request->province_id;
            $address->long = $request->long;
            $address->lat = $request->lat;
            $address->discraption = $request->discraption;
            $address->save();
            $address_id = $address->id;
            $user= new Client;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password =  app('hash')->make($request->password);
            $user->phone = $request->phone;
            $user->address_id = $address_id;
            $user->token = $request->token;
            $user->img_path = $path[0];
            $user->save();
            $Data = $user->id;
        }catch(\Illuminate\Database\QueryException $e){
            //var_dump($e->errorInfo);
            $Data = null;
        }
        if(empty($Data)){
            return response()->json([
                'success' => false,
                'Data' => null
            ], 400);
        }else{
            return response()->json([
                'success' => true,
                'Data' => null
            ], 201);
        }
    }
}

