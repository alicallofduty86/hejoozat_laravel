<?php

namespace App\Http\Controllers\Auth;
use App\Address;
use App\User;
use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function createUser(array $data)
    {   
        $address = new Address;
        $address->country_id = $data['country_id'];
        $address->province_id = $data['province_id'];
        $address->long = 1;
        $address->lat = 1;
        $address->discraption = 1;
        $address->save();
        $address_id = $address->id;
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => 00,
            'status'=>1,
            'owner_id' => 1,
            'role_id' => 1,
            'address_id' => $address_id,
            'token' => NULL, 
            'img_path' => 'hhhhhhhhhhhhhhh'
        ]);
    }

    protected function createClient(array $data)
    {   
        $address = new Address;
        $address->country_id = $data['country_id'];
        $address->province_id = $data['province_id'];
        $address->long = 1;
        $address->lat = 1;
        $address->discraption = 1;
        $address->save();
        $address_id = $address->id;
        return Client::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => 00,
            'status'=>1,
            'address_id' => $address_id,
            'token' => NULL, 
            'img_path' => 'hhhhhhhhhhhhhhh'
        ]);
    }
}
