<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Book\ReservationController as Book;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Session;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'client/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest')->except('logout');
       // $this->middleware('guest:client')->except('logout');
    }

    public function clientLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('client')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
           // dd(Auth::guard('client')->user()->name);
           $obj = new Book;
           $count =  $obj->getAllCount();
            return redirect()->back();
        }
        Session::flash('Title', 'مشكلة في تسجيل الدخول'); 
        return redirect('client/home'); 
    }

    public function ownerLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
           // dd(Auth::guard('client')->user()->name);
        /*   $obj = new Book;
           $count =  $obj->getAllCount();*/
            return redirect('dash');
        }
        Session::flash('Title', 'مشكلة في تسجيل الدخول'); 
        return redirect()->back(); 
    }
}
