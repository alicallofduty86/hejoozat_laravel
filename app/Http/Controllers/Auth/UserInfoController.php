<?php
namespace App\Http\Controllers\Auth;
use Validator;
use App\User;
use App\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Routing\Controller as BaseController;
class UserInfoController extends BaseController 
{
    public function signUp(request $request){
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'phone' => 'required',
            'owner_id' => 'required',
            'role_id' => 'required',
            'province_id' => 'required',
            'country_id' => 'required',
            'token' => 'required',
            'img_path' =>'required'
            ]);

        $Data = null;
        try{
            $address = new Address;
            $address->country_id = $request->input('country_id');
            $address->province_id = $request->input('province_id');
            $address->long = $request->input('long');
            $address->lat = $request->input('lat');
            $address->discraption = $request->input('discraption');
            $address->save();
            $address_id = $address->id;
            $user= new User;
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password =  app('hash')->make($request->input('password'));
            $user->phone = $request->input('phone');
            $user->owner_id = $request->input('owner_id');
            $user->role_id = $request->input('role_id');
            $user->address_id = $address_id;
            $user->token = $request->input('token');
            $user->img_path = $request->input('img_path');
            $user->save();
            $Data = $user->id;
        }catch(\Illuminate\Database\QueryException $e){
            //var_dump($e->errorInfo);
            $Data = null;
        }
        if(empty($Data)){
            return response()->json([
                'success' => false,
                'Data' => null
            ], 400);
        }else{
            return response()->json([
                'success' => true,
                'Data' => null
            ], 201);
        }
    }
}

