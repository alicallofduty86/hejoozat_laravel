<?php
namespace App\Http\Controllers\Auth;
use Validator;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;
class AuthController extends BaseController 
{
    
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }
    /**
     * Create a new token.
     * 
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 
    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\User   $user 
     * @return mixed
     */
    public function authenticate(User $user) {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);
        // Find the user by email
        $user = User::where('email', $this->request->input('email'))->first();
        if (!$user) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'success'=> false,
                'error' => 'Email does not exist.'
            ], 400);
        }
        // Verify the password and generate the token
        if (app('hash')->check($this->request->input('password'), $user->password)) {
            return response()->json([
                'success'=> true,
                'token' => $this->jwt($user),
                'role' => $user->role_id
            ], 200);
        }
        $u = app('hash')->make($this->request->input('password'));
        // Bad Request response
        return response()->json([
            'success'=> false,
            'error' => 'password is wrong.'
        ], 401);
    }

    public function refresh(){
        $token = $this->request->get('token');

        if(!$token){
            return response()->json([
                'Result' => false,
                'Msg' => 'Token not provided'
            ],401);
        }
        try{
        $credentials = JWT::decode($token , env('JWT_SECRET'),['HS256']);
        }catch(ExpiredException $e){
            $ar = array();
            $r = explode('.',$token);
            //echo base64_decode( $r[1]);
            $userId = json_decode(base64_decode( $r[1]),true)['sub'];
            $user = User::where('id', $userId)->first();
            if (!$user) {
                return response()->json([
                    'success'=> false,
                    'error' => 'Token does not exist.'
                ], 400);
            }
            return response()->json([
                'success'=> true,
                'token' => $this->jwt($user)
            ], 200);

        }catch(Exception $e) {
            return response()->json([
                'error' => 'An error while decoding token.'
            ], 400);
        }

        return response()->json([
            'error' => 'Token Not Expired'
        ], 400);
    }
}