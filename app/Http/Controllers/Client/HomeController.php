<?php

namespace App\Http\Controllers\Client;
use App\Images;
use App\Item;
use App\ItemPrice;
use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
      // $this->middleware('client')->except('logout');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    { 
        $item = $this->getLast();
        $cat = $this->getCategory();
        return view('client/home')->with(['item'=>$item,'cat'=>$cat]);
    }

    public function getLast(){
        $item = Item::with('item_price','time_to_open','start_end','images','owner')
        ->orderBy('created_at','desc')
        ->paginate(6);
        return $item;
    }

    public function getResult(Request $request){
        $disc = $request->disc;
        $id = $request->id;
        $provinceId = $request->province;

        $item = Item::with(['item_price','time_to_open','start_end','images','owner' => function($query) use($provinceId){
            $query->join('address','owner.address_id','=','address.id')
            ->where('province_id',$provinceId);
        } ])
        ->where('sub_category_id','=',$id)
        ->orWhere('disc','like','%'.$disc.'%')
        ->orWhere('name','like','%'.$disc.'%')
        ->get();
        $filtered = $item->where('owner','!=',null);
        $i = $filtered->all();
       // return $i;
         return view('client/result')->with(['item'=>$i]);       
     }

     public function getCategory(){
        $category = new Category;
        $all = $category->all();
        return $all;
       }
   
       public function getSubCategory($id){
           $subCategory = SubCategory::where('category_id','=',$id)->get();
           return $subCategory;
       }
}
