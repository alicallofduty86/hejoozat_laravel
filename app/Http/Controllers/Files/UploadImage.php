<?php

namespace App\Http\Controllers\Files;
use Validator;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadImage extends BaseController
{
    private $Path = [null];
    private $Path1 = [null];
    public function upload(Request $request){
        $this->validate($request, [
            'mainFolder' => 'required',
            'folder' => 'required',
            'type' => 'required',
            'images' => 'required|array|min:1|max:4'
            ]);
     
            $this->Create_Path($request->input('mainFolder'),$request->input('folder'),count($request->input('images')),$request->input('type'));
            if($this->UploadAtt($request->input('images')) == true){
                return response()->json([
                    'success' => true,
                    'Data' => $this->Path
                ],201);
            }else{

            }
 
     }
     public function uploadInner(Request $request){
            $this->Create_Path($request->input('mainFolder'),$request->input('folder'),count($request->input('images')),$request->input('type'));
            if($this->UploadAtt($request->input('images')) == true){
               return $this->Path1;
            }else{

            }
 
     }
     function Create_Path($MainFolder,$Folder,$Images,$Type){
        $path = ".././storage/attachments/$MainFolder /$Folder";
       // echo $path;
        if (!file_exists(".././storage/attachments/$MainFolder"))
             mkdir(".././storage/attachments/$MainFolder");
        if(!file_exists(".././storage/attachments/$MainFolder/$Folder"))
            mkdir(".././storage/attachments/$MainFolder/$Folder");
        $xx = null;
        for($i = 0;$i < $Images;$i++){
        $micro =(int)microtime(1); 
        $name=$micro. $xx .".".$Type;
        $arr=$path. "/" .$name; 
        $xx.="x";
        $arr1 = str_replace(".././", "/", $arr);
        $this->Path1[$i] = $arr1;
        $this->Path[$i] = $arr;
        }
       // print_r($this->Path);
        return true;
        }

        function UploadAtt($DataFile){
            if(!empty($this->Path)){
                $CountFile = count($this->Path);
                $u = 0;
                for($u ;$u < $CountFile ;$u++){
                    $Data = base64_decode($DataFile[$u]);
                    if(file_put_contents($this->Path[$u], $Data)){
                        $Data = null;
                    }
                }
                return true;
            }else{
            return false;
          }
        }
}