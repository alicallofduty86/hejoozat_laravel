<?php
namespace App\Http\Controllers\Book;
use Validator;
use App\Reservation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller as BaseController;
use Auth;
use Redirect;
use Session;
use Uuid;
class ReservationController extends BaseController 
{
     public function __construct(){

    }
    public function set(request $request){
   
            //dd($request->all());
        $Data = null;
        try{
            $Reservation = new Reservation;
            $Reservation->client_id = Auth::guard('client')->user()->id;
            $Reservation->item_id = $request->input('item_id');
            $Reservation->from_date = $request->input('from_date');
            $Reservation->to_date = $request->input('to_date');
            $Reservation->from_hour = $request->input('from_hour');//$request->input('from_date');
            $Reservation->to_hour = $request->input('to_hour');//$request->input('to_date');
            $Reservation->is_am = (($request['am'] == null || $request['am'] == false) ? false :true);
            $Reservation->number = 1;//$request->input('number');
            $Reservation->serial_number = (string)Uuid::generate();
            $Reservation->active = 1;
            $Reservation->confirm = 0;
            $Reservation->save();
            $Data = $Reservation->id;
            $this->getAllCount();
        }catch(\Illuminate\Database\QueryException $e){
            dd($e->errorInfo);
            $Data = null;
        }
        if(empty($Data)){
             return redirect()->back()->with('error','لم يتم الحجز');
        }else{
            Session::put('success','تم الحجز بنجاح');
             return redirect()->back()->with('success','تم الحجز بنجاح');
        }
    }

    public function getAll()
    {
        $Item = Reservation
        ::join('items','reservation.item_id',"=","items.id")
        ->join('item_price','reservation.item_id',"=","item_price.item_id")
        //->join('owner','items.owner_id',"=","owner.id")
        ->select('reservation.id','reservation.number','reservation.serial_number','reservation.active','reservation.from_date','reservation.to_date','reservation.from_hour','reservation.to_hour','reservation.created_at','reservation.confirm','items.name','item_price.price','item_price.deduction')
        ->where('reservation.client_id','=',Auth::guard('client')->user()->id)
        ->where('reservation.active','!=','2')
        ->getQuery()
        ->get();
        $this->getAllCount();
        return view('client/bookedList')->with('items',$Item);
    }

    public function getOrderList()
    {
        //must select owner_id from user
        $Item = Reservation
        ::join('items','reservation.item_id',"=","items.id")
        ->join('item_price','reservation.item_id',"=","item_price.item_id")
        //->join('owner','items.owner_id',"=",Auth::guard('web')->user()->owner_id)
        ->select('reservation.id','reservation.from_date','reservation.serial_number','reservation.to_date','reservation.from_hour','reservation.to_hour','reservation.is_am','reservation.number','reservation.active','reservation.created_at','reservation.confirm','items.name','item_price.price','item_price.deduction')
        ->where('reservation.active','!=','2')
        ->where('items.owner_id',"=",Auth::guard('web')->user()->owner_id)
        ->getQuery()
        ->get();
        $this->getAllOrdersCount();
        return view('owner/orderList')->with('items',$Item);
    }

    public function getAllCount()
    {
        $Item = Reservation
        ::join('items','reservation.item_id',"=","items.id")
        ->join('item_price','reservation.item_id',"=","item_price.item_id")
        //->join('owner','items.owner_id',"=","owner.id")
        ->select('reservation.id','reservation.number','reservation.created_at','reservation.confirm','items.name','item_price.price','item_price.deduction')
        ->where('reservation.client_id','=',Auth::guard('client')->user()->id)
        ->where('reservation.active','!=','2')
        ->getQuery()
        ->get();
        $count = count($Item);
        Session::put('count',$count);
        return $count;
    }

    public function getAllOrdersCount()
    {
        //must select owner_id from user
        $Item = Reservation
        ::join('items','reservation.item_id',"=","items.id")
        ->join('item_price','reservation.item_id',"=","item_price.item_id")
        //->join('owner','items.owner_id',"=",Auth::guard('web')->user()->owner_id)
        ->select('reservation.id','reservation.number','reservation.created_at','reservation.confirm','items.name','item_price.price','item_price.deduction')
        ->where('reservation.active','!=','2')
        ->where('items.owner_id',"=",Auth::guard('web')->user()->owner_id)
        ->getQuery()
        ->get();
        $count = count($Item);
        Session::put('count',$count);
        return $count;
    }


    public function getBy($id)
    {
        $Item = Reservation
        ::join('items','reservation.item_id',"=","items.id")
        ->join('item_price','reservation.item_id',"=","item_price.item_id")
        ->join('Images','reservation.item_id',"=","Images.item_id")
        ->where("reservation.item_id","=",$id)
        ->getQuery()
        ->get();
         return redirect()->back()->with('item',$Item);
    }

    public function cancelReservation(request $request){
        $Reservation = Reservation::find($request->input('id'));
        $Reservation->active = 2;
        $Reservation->save();
        $this->getAllCount();
        return redirect()->back()->with('success','تم اللغاء الحجز بنجاح');
    }
    public function OwnerCancelReservation(request $request){
        $Reservation = Reservation::find($request->input('id'));
        $Reservation->active = 3;
        $Reservation->save();
        //$this->getAllCount();
        Session::put('success','تم اللغاء الحجز بنجاح');
        return redirect()->back()->with('success','تم اللغاء الحجز بنجاح');
    }

    public function confirmReservation(request $request){
        $Reservation = Reservation::find($request->input('id'));
        $Reservation->confirm = 1;
        $Reservation->save();
       // $this->getAllCount();
        return redirect()->back()->with('success','تم الموافقة بنجاح');
    }

    public function edit(request $request){
        $this->validate($request, [
            'client_id' => 'required',
            'item_id' => 'required',
            'from_date' => 'required',
            'to_date' => 'required',
            'number' => 'required',
            ]);
        $Data = null;
        try{
            $Reservation = Reservation::find($request->input('id'));
            $Reservation->client_id = $request->input('client_id');
            $Reservation->item_id = $request->input('item_id');
            $Reservation->from_date = $request->input('from_date');
            $Reservation->to_date = $request->input('to_date');
            $Reservation->number = $request->input('number');
            $Reservation->serial_number = $request->input('serial_number');
            $Reservation->active = 1;
            $Reservation->confirm = 0;
            $Reservation->save();
            $Data = $Reservation->id;
        }catch(\Illuminate\Database\QueryException $e){
            //var_dump($e->errorInfo);
            $Data = null;
        }
        if(empty($Data)){
           return redirect()->back()->with('success','تم تعديل البيانات بنجاح');
        }else{
            return redirect()->back()->with('error','لم يتم تعديل البيانات');
        }
    }
}

