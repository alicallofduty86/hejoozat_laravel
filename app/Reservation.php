<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Reservation extends Model
{
    protected $table = 'reservation';

    protected $primaryKey = 'id';
    protected $dates = ['created_at','updated_at'];

    protected $fillable = ['client_id','item_id','number'
                           ,'from_date','to_date','active'
                           ,'serial_number','confirm','from_hour','to_hour','is_am'];
   
    
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
}