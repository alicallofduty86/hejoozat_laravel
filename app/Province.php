<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Province extends Model
{
    protected $table = 'Province';

    protected $primaryKey = 'id';

    protected $fillable = ['name','country_id'];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
   

}