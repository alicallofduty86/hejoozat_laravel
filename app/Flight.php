<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Flight extends Model 
{

    protected $table = 'flight';

    protected $primaryKey = 'id';

    protected $fillable = [
        'item_id',
        'from_province_id',
        'to_province_id',
        'one_way',
        'two_way',
        'adults',
        'childrenn',
        'Youths',
        'seat_infants',
        'lap_infants',
        'class',
        'airport',
        'company_flight_id'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function from_province()
    {
        return $this->belongsTo('App\Province');
    }

    public function to_province()
    {
        return $this->belongsTo('App\Province');
    }

    public function company_flight()
    {
        return $this->belongsTo('App\CompanyFlight');
    }

}