<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Device extends Model 
{

    protected $table = 'device';

    protected $primaryKey = 'id';

    protected $fillable = [
        'item_id',
        'color',
        'size',
        'smart',
        'stellite',
        'threeD',
        'curve',        
        'brand_id',
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

}