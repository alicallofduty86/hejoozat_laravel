<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TimeToOpen extends Model 
{

    protected $table = 'time_to_open';

    protected $primaryKey = 'id';

    protected $fillable = ['item_id','from_day','to_day','from_time','to_time','every_day','every_time'];
    public function item()
    {
        return $this->belongsTo('App\Item');
    }



}