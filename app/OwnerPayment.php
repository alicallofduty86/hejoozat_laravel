<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OwnerPayment extends Model
{

    protected $table = 'owner_payment';

    protected $primaryKey = 'id';

    protected $fillable = ['owner_id','payment_id','number'];

    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }

    public function owner()
    {
        return $this->belongsTo('App\Owner');
    }
   

}