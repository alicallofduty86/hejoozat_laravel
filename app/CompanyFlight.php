<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CompanyFlight extends Model
{

    protected $table = 'company_flight';

    protected $primaryKey = 'id';

    protected $fillable = ['name','country_id','img_path'];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
   

}