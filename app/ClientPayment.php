<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ClientPayment extends Model
{

    protected $table = 'client_payment';

    protected $primaryKey = 'id';

    protected $fillable = ['client_id','payment_id','number'];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }


}