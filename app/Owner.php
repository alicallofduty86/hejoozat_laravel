<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{

    protected $table = 'Owner';

    protected $primaryKey = 'id';

    protected $fillable = ['name','status','category_id','img_path','address_id'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function address()
    {
        return $this->belongsTo('App\Address');
    }
     
   

}