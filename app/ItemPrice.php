<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ItemPrice extends Model 
{

    protected $table = 'item_price';

    protected $primaryKey = 'id';

    protected $fillable = ['item_id','price','book_without_credit_card',
    'free_cancellation','deduction','no_prepayment',
    'cancellation_deduction_ratio','status'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
    



}