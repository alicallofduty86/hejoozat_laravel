<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Image extends Model
{

    protected $table = 'Images';

    protected $primaryKey = 'id';

    protected $fillable = ['item_id','image_path'];

    public function getImage_PathAttribute($image_path){

        return asset($image_path);
    }

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
   

}