<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $primaryKey = 'id';
    protected $fillable = [
        'password','email','name','phone','address_id','owner_id','role_id','status','token','img_path'
    ];

    /*protected $hidden = [
        'password','email'
    ];*/

    public function address()
    {
        return $this->belongsTo('App\Address');
    }

    public function owner()
    {
        return $this->belongsTo('App\Owner');
    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }
}

