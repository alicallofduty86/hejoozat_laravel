<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class HotelPark extends Model 
{

    protected $table = 'hotel_park';

    protected $primaryKey = 'id';

    protected $fillable = [
        'item_id',
        'adults',
        'childrenn',
        'number_of_room',
       'family_room',
       'double_bed',
       'twin_beds',
       'swimming_pool',
       'sauna',
       'breakfast_included',
       'fitness_center',
       'dinner_included',
       'self_catering',
       'free_wifi',
       'parking',
       'shuttle',
       'private_bathroom',
       'balcony',
       'flat_screen_tv',
       'air_conditioning',
       'hairdryer',
       'wellness_centre',
       'upper_floors_accessible_by_lift'
    
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

}