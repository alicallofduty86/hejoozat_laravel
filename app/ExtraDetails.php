<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ExtraDetails extends Model
{

    protected $table = 'extra_details';

    protected $primaryKey = 'id';

    protected $fillable = ['key','value','item_id'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
   

}