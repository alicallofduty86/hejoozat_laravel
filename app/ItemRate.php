<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ItemRate extends Model
{

    protected $table = 'item_rate';

    protected $primaryKey = 'id';

    protected $fillable = ['item_id','value'];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }
   

}