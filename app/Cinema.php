<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cinema extends Model 
{

    protected $table = 'cinema';

    protected $primaryKey = 'id';

    protected $fillable = [
        'item_id',
        'main_actor',
        'movie_date',
        'food_included',
        'vip',
        '3d',
        'normal',
        'time_to_open_id',
    ];

    public function item()
    {
        return $this->belongsTo('App\Item');
    }

    public function time_to_open()
    {
        return $this->belongsTo('App\TimeToOpen');
    }

}