<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OwnerRate extends Model
{

    protected $table = 'ownerrate';

    protected $primaryKey = 'id';

    protected $fillable = ['owner_id','value'];

    public function owner()
    {
        return $this->belongsTo('App\Owner');
    }
   

}