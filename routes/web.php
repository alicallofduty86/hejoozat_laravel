<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('qr', function () { 
    return QrCode::size(500)->generate('923388990'); 
});
Route::get('/detail', function () {
    return view('client/details');
});

Route::get('/reg', function () {
    return view('client/register');
});

Route::get('/add', function () {
    return view('owner/addHotel');
});
Route::get('/addAny', function () {
    return view('owner/addAny');
});

Route::get('/addMedical', function () {
    return view('owner/addMedical');
});

Route::get('/dash', function () {
    return view('owner/dashboard');
});


Route::get('/owner', function () {
    return view('owner/login');
});

Auth::routes();
Route::post('/logoutOwner', '\App\Http\Controllers\Auth\LogoutController@logoutOwner')->name('logoutOwner');
Route::post('/clientLogout', '\App\Http\Controllers\Auth\LogoutController@logout')->name('clientLogout');
Route::post('/addHotel', '\App\Http\Controllers\Items\HotelParkController@store')->name('addHotel');
Route::post('/addMedical', '\App\Http\Controllers\MedicalController@store')->name('addMedical');
Route::post('/addAny', '\App\Http\Controllers\Items\AnyController@store')->name('addAny');
Route::post('/confirmBooked', 'Book\ReservationController@confirmReservation')->name('confirmBooked');
Route::post('/bookedCancelOwner', 'Book\ReservationController@OwnerCancelReservation')->name('bookedCancelOwner');
Route::get('/orderList', 'Book\ReservationController@getOrderList')->name('orderList');

Route::group(['middleware' => ['admin']], function () {
    Route::get('/test', function () {
        return view('test');
    });
});



Route::group(['prefix' => 'client'], function () {
    Route::get('/search', function () {
        return view('client/result');
    });
    Route::get('/home', function () {
        return view('client/home');
    });
    Route::get('/home', '\App\Http\Controllers\Client\HomeController@index')->name('home'); 
    Route::post('/search', 'Client\HomeController@getResult')->name('search');
    Route::post('/getById', 'Items\SearchController@getById')->name('getById');

    Route::get('/bookedListCount','Book\ReservationController@getAllCount')->name('bookedListCount');
    Route::get('/bookedList','Book\ReservationController@getAll')->name('bookedList');
    //Route::post('/booking', 'Book\ReservationController@set')->name('booking');
    Route::post('/bookedCancel', 'Book\ReservationController@cancelReservation')->name('bookedCancel');
});
Route::post('/booking', 'Book\ReservationController@set')->name('booking');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::post('/ownertLogin', 'Auth\LoginController@ownerLogin')->name('ownertLogin');
Route::post('/clientLogin', 'Auth\LoginController@clientLogin')->name('clientLogin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'getInfo'], function ($app) {
    $app->get('/Address/{id}',[
        'uses' => 'Info\AddressController@getAddress'
    ]);
    $app->get('/Country',[
        'uses' => 'Info\AddressController@getCountry'
    ]);
    $app->get('/Province/{id}',[
        'uses' => 'Info\AddressController@getProvince'
    ]);
    $app->get('/Category',[
      'uses' => 'Info\CategoryInfo@getCategory'
    ]);
    $app->get('/SubCategory/{id}',[
        'uses' => 'Info\CategoryInfo@getSubCategory'
    ]);
});
